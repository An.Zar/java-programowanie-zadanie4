package pl.sda;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String tekst = sc.nextLine();

        String wyciencie = tekst.substring(tekst.length()-3);
        String wycieciePoczatek = tekst.substring(0,tekst.length()-3);
        String duzeLitery = wyciencie.toUpperCase();
        String tekstPoprawiony = wycieciePoczatek + duzeLitery;

        System.out.println(tekstPoprawiony);

    }
}
